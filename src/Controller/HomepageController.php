<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;

Class HomepageController extends AbstractController
{

    /*
        annotations
            utiliser uniquement des doubles guillemets
            commentaires lus par symfony
            @Route : paramètres
                schéma de la route : url saisie 
                name : unique de la route
                    nomenclature : nom du contrôleur . nom de la méthode

        récupération de la requete HTTP :
            lister les services : symfony console debug:container
            injecter un service :
                créer une propriété qui va stocker le service
                créer un constructeur avec un paramètre du même type que le service
                dans le constructeur : lien en la propriété et le paramètre
    */

    private $requestStack;
    private $request;


    
    public function __construct(RequestStack $requestStack)
    {
        $this->requestStack=$requestStack;
        $this->request=$this->requestStack->getCurrentRequest();
    }

    /**
     * @Route("/", name="homepage.index")
    */
    public function index():Response
    {
        /*
            débogage :
                dd : dump and die
                dump : affichage dand la barre de débogage de symfony
            
            Request :
                propriété request : $_POST
                propriété query : $_GET
                propriété files : $_FILES
                propriété header : en-têtes
        */
        //dd($this->request->headers->get('accept-language'));
        //$response =  new Response('coucou');
        //return $response;

        /*
            vue Twig:
                créer un dossier du même nom que le controleur dans le dossier "templates"
                dans le dossier, créer un fichier du même nom que la méthode, avec une extensin en html.twig
            méthode render : appel d'une vue twig à partir du dossier "templates"
        */
        return $this->render('homepage/index.html.twig');

    }

}