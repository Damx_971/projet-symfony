<?php

namespace App\Controller;

use App\Repository\CategoryRepository;
use App\Repository\GifRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class SearchController extends AbstractController
{
    private Request $request;
    private CategoryRepository $categoryRepository; 
    private GifRepository $gifRepository;

    public function __construct(CategoryRepository $categoryRepository, GifRepository $gifRepository)
    {
        $this->categoryRepository = $categoryRepository;
        $this->gifRepository = $gifRepository;
    }
    
    /**
     * @Route("/search", name="search.index")
     */

    public function index(Request $request)
    {
        $params = $request->get('search-content');
        $result = $this->categoryRepository->search($params)->getResult();
        $e = 0;
    
        for($i = 0; $i<sizeof($result); $i++)
        {
            if($this->gifRepository->gifSearch($result[$e]->getId())->getResult()!=Null){

                $gifs[$i] = $this->gifRepository->gifSearch($result[$e]->getId())->getResult();
            }
           $e++;
        }

        return $this->render('search/index.html.twig',['gifs'=>$gifs]);
    }
}
