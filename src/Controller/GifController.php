<?php

namespace App\Controller;

use App\Repository\CategoryRepository;
use App\Repository\GifRepository;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

Class GifController extends AbstractController
{
    private GifRepository $gifRepository;

    public function __construct(GifRepository $gifRepository)
    {
        $this->gifRepository = $gifRepository;
    }

    /*
        création de variable de route:
            utilisation d'accolades dans le schéma de la route 
            la variable de route se retrouve en paramètre de la méthode
    */

    /**
     * @Route("/gif/{gifSlug}", name="gif.index")
    */
    public function gif(string $gifSlug): Response
    {
        $findgif = $this->gifRepository->findOneBy([
            'slug'=>$gifSlug
        ]);
        //dd($findcategory);

        return $this->render('gif/index.html.twig',[
            'gif'=>$findgif
        ]);
    }

}