<?php

namespace App\DataFixtures;

use App\Entity\Category;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\String\Slugger\AsciiSlugger;

class CategoryFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $slugger = new AsciiSlugger();
        foreach(AbstractDataFixtures::CATEGORIES as $main => $sub){
            $mainCategory = new Category();
            $mainCategory
                ->setName($main)
                ->setSlug($slugger->slug($main))
            ;

            // méthode persite de doctrine : équivaut à  insert into (met les requêtes en file d'atentes pour les excuter)
                // $product = new Product();
                // $manager->persist($product);

            $manager->persist($mainCategory);

            // création des sous-catégories
            foreach($sub as $subcategory){
                $subcat = new Category;
                $subcat
                    ->setName($subcategory)
                    ->setSlug($slugger->slug($subcategory))
                    ->setParent($mainCategory)
                ;
                $manager->persist($subcat);

                /* stockage en mémoire des entité pour y accéder dans d'autres fixtures
                 addReference : 2 paramètres
                    identifiant unique de la référence
                    entité liée à la référence
                */
                $this->addReference("subcategory$subcategory", $subcat);
            }
        }
        //méthode flush de doctrine : permet d'éxécuter les requêtes mis en file d'atentes par la méthode persite)
        $manager->flush();
    }
}
